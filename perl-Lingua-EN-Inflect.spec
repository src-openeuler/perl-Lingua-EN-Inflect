Name:           perl-Lingua-EN-Inflect
Version:        1.905
Release:        2
Summary:        Convert singular to plural, select "a" or "an"
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Lingua-EN-Inflect
Source0:        Lingua-EN-Inflect-1.905.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Run-time:
BuildRequires:  perl(Carp)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(vars)
# Tests:
BuildRequires:  perl(Test::More)
Requires:  perl(Carp)

%description
%{summary}.

%prep
%setup -q -n Lingua-EN-Inflect-%{version}
chmod -x Changes README lib/Lingua/EN/Inflect.pm

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/Lingua/
%{_mandir}/man3/*.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.905-2
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Jul 8 2021 chenjian <chenjian@kylinos.cn> - 1.905-1
- Init package

